var app = new Vue({
    el:'#app',
    data:{
    dictionary:{},
    message:'',
},
    methods: { 
        doSomething: function(){
            var text = document.querySelector('textarea').value
            var div = document.getElementById('output')
            if (text !== '') {
  
                div.innerHTML = ''
            
                // var dictionary = {}
            
                text.replace(/\S+/g,function(word){
                  word = word.split('.')[0]
                  word = word.split(',')[0]
                  word = word.toLowerCase()
                  if (this.dictionary[word] == undefined){
                    this.dictionary[word] = 1
                  } else {
                    this.dictionary[word]++
                  }
                })
            
                 this.message = '<table data-theme="zebra dark"><thead><tr><th>Word<th>Count</thead></tr><tbody>'
            
                for (word in dictionary) {
                  this.message += '<tr><td>' + word + '<td>' + this.dictionary[word] + '</tr>'
                }
            
                this.message += '</tbody></table>'
            
                div.innerHTML = this.message
            
                $('table').DataTable({
                  "order":[[1,'desc']]
                })
            
              } else {
              
                notification('Please enter at least one word', 'error', 5)
            
              }

        },
     },
}) ;