var app = new Vue({
    el:'#app',
    data:{
        div: '',
        dictionary: {},
        message:'',
},
    methods: { 
        doSomething: function(){
            var text = document.getElementById('txt').value;
                this.div = '';
                text.replace(/\S+/g,function(word){
                    word = word.split('.')[0]
                    word = word.split(',')[0]
                    word = word.toLowerCase()
                    if (this.dictionary[word] == undefined){
                      this.dictionary[word] = 1
                    } else {
                      this.dictionary[word]++
                    }
                  })
        },
     },
}) ;