var app = new Vue({
    el:'#app',
    data:{
    
},
    methods: { 
        doSomething: function(){
            var text = document.getElementById('imp').value;
            var div = document.getElementById('output');
            if(text !== ''){
                div.innerHTML = '';
                var dictionary = {}
                
                text.replace(/\S+/g,function(word){
                    word = word.split('.')[0]
                    word = word.split(',')[0]
                    word = word.toLowerCase()
                    if (dictionary[word] == undefined){
                      dictionary[word] = 1
                    } else {
                      dictionary[word]++
                    }
                  })

                  var message = '<table data-theme="zebra dark"><thead><tr><th>Word<th>Count</thead></tr><tbody>';
                  for (word in this.dictionary) {
                    message += '<tr><td>' + word + '<td>' + this.dictionary[word] + '</tr>';
                  }
                  message += '</tbody></table>';

                  div.innerHTML = message;

                  $('table').DataTable({
                    "order":[[1,'desc']]
                  })
                }else{
                    notification('Please enter at least one word', 'error', 5)
                }

        },
     },
}) ;